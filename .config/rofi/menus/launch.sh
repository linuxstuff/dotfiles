#!/bin/bash

# Run with menu name to display a rofi menu
# Example: "sh launch.sh defaultsink"

rofi -modi menu:/home/sim/.config/rofi/menus/"$1".py -show menu