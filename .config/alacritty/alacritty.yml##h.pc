title: Terminal

env:
  TERM: xterm-256color

window:
  opacity: 0.8
  dynamic_padding: true
  padding:
    x: 30
    y: 30

cursor:
  hide_when_typing: true

font:
  normal:
    family: Hack Nerd Font
    style: Regular

  bold:
    family: Hack Nerd Font
    style: Bold

  italic:
    family: Hack Nerd Font
    style: Italic

  bold_italic:
    family: Hack Nerd Font
    style: Bold Italic

  size: 11.0

  offset:
    x: 0
    y: -1

key_bindings:
  - { key: Return,   mods: Control|Shift, action: SpawnNewInstance } # Spawn new instance in same directory

schemes:

  xrdb: &xrdb
    primary:
      background: '#2e3440'
      foreground: '#eceff4'
    normal:
      black:   '#3b4252'
      red:     '#bf616a'
      green:   '#a3be8c'
      yellow:  '#ebcb8b'
      blue:    '#81a1c1'
      magenta: '#b48ead'
      cyan:    '#88c0d0'
      white:   '#eceff4'
    bright:
      black:   '#3b4252'
      red:     '#bf616a'
      green:   '#a3be8c'
      yellow:  '#ebcb8b'
      blue:    '#81a1c1'
      magenta: '#b48ead'
      cyan:    '#88c0d0'
      white:   '#eceff4'

  nord: &nord
    primary:
      background: '#171717'
      foreground: '0xD8DEE9'
    normal:
      black:   '0x3B4252'
      red:     '0xBF616A'
      green:   '0xA3BE8C'
      yellow:  '0xEBCB8B'
      blue:    '0x81A1C1'
      magenta: '0xB48EAD'
      cyan:    '0x88C0D0'
      white:   '0xE5E9F0'
    bright:
      black:   '0x4C566A'
      red:     '0xBF616A'
      green:   '0xA3BE8C'
      yellow:  '0xEBCB8B'
      blue:    '0x81A1C1'
      magenta: '0xB48EAD'
      cyan:    '0x8FBCBB'
      white:   '0xECEFF4'

#colors: *nord
colors: *xrdb
