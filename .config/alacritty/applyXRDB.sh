#!/bin/bash

###########
# Update alacritty config to apply Xresources color scheme
#

NAMED_COLORS=("foreground" "background")

# Target file
SKELETON_FILE="$HOME/.config/alacritty/alacritty.skeleton.yml"
TARGET_FILE="$HOME/.config/alacritty/alacritty.yml"

# copy input file to temporary file for black magic fuckery
# (alacritty applies colors when the config file is written, so we want to do it
# all in one write)
cp "$SKELETON_FILE" "$TARGET_FILE.tmp"

# Grab colors from Xresources
xrdb ~/.Xresources

# Named colors
for i in "${NAMED_COLORS[@]}"
do
    color=$(xrdb -query | awk "/*.$i/ { print substr(\$2,2) }")
    sed -i "s/%$i%/#${color}/g" "$TARGET_FILE.tmp"
done

# Numbered colors
for i in {0..15}
do
    v=$(xrdb -query | awk '/*.color'"$i":'/ { print substr($2,2) }')
    eval "sed -i 's/%color${i}%/#${v}/g' $TARGET_FILE.tmp";
done


# Finally, replace target file with our updated one
rm -f "$TARGET_FILE"
mv "$TARGET_FILE.tmp" "$TARGET_FILE"